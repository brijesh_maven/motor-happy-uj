<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['namespace' => 'Api'], function () {

    Route::get('/auth/social', 'AuthController@social');

    Route::get('/user', function (Request $request) {
        return $request->user()->with('social_profiles')->first();
    })->middleware('auth:api');

});


