<?php

namespace App\Http\Controllers\Api;

use App\Models\SocialProfile;
use App\Models\User;
use App\Social\SocialSdk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/**
 * Class AuthController
 * @package App\Http\Controllers\Api
 */
class AuthController extends BaseApiController
{
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        //$this->middleware('auth:api');
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function social(Request $request)
    {


        // Verify social login/register request for allowed social profiles
        if(!in_array($request['profile'], config('social.available_profiles'))){
            $message = ($request['profile']) ?
                $request['profile'] . ' not allowed to login.' :
                'profile name is required.';
            return $this->respondWithError($message);
        }

        // Check if token is provided
        if(!$request['token']){
            return $this->respondWithError('Token is required.');
        }

        // get social user from token
        $socialsdk = new SocialSdk($request['profile'], $request['token']);
        $social_user = $socialsdk->get_user();

        // Check for Error while fetching social user
        if($social_user['status'] === false){
            return $this->respond([
                'error' => $social_user['error'] ? $social_user['error'] : $social_user
            ]);
        }

        // Check if Social User have required permissions
        $check_permissions = $socialsdk->check_permissions($social_user['data']['permissions']);
        if($check_permissions['status'] == false)
        {
            return $this->respond([
                'error'     => $check_permissions['error']  ? $check_permissions['error'] : $check_permissions['error']
            ]);
        }

        // Create/Update Social Profile with User.
        $user_id = $this->save_social_profile($social_user['data']);

        //Return User with full details including all social profiles.
        return $this->respond([
            'user'   => User::where('_id', $user_id)->with('social_profiles')->first()
        ]);

    }


    /**
     * @param $social_user
     * @return mixed
     */
    private function save_social_profile($social_user)
    {
        $socialprofile = SocialProfile::where([
            'profile_id' => $social_user['id'],
            'profile_name' => $social_user['profile']
        ])->first();

        if(!$socialprofile)
        {
            $socialprofile = new SocialProfile();
        }
        $socialprofile->profile_name = $social_user['profile'];
        $socialprofile->profile_id = $social_user['id'];
        $socialprofile->profile_email = $social_user['email'];
        $socialprofile->profile_token = $social_user['token'];
        $socialprofile->save();


        // Create / Update User
        if($user = User::where('_id', $socialprofile->user_id)->first()){}
        else if($user = User::where('primary_email', $socialprofile->profile_email)->first()){}
        else
        {
            $user = new User;
            $user->first_name = $social_user['first_name'];
            $user->last_name = $social_user['last_name'];
            $user->primary_email = $social_user['email'];
            $user->api_token = Password::getRepository()->createNewToken();
            $user->picture = $social_user['picture_url'];
            $user->role = 'user';
            $user->save();
        }
        // assign Social profile to User
        $user->social_profiles()->save($socialprofile);

        return $user->_id;


    }


}
