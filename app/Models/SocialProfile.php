<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class SocialProfile
 * @package App\Models
 */
class SocialProfile extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'profile_name', 'profile_email', 'profile_id', 'profile_token', 'user_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'profile_token',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
