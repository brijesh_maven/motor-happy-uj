<?php


namespace App\Social\Sdks;


use App\Social\Interfaces\SocialSdkContract;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

/**
 * Class FacebookSdk
 * @package App\Social\Sdks
 */
class FacebookSdk extends Facebook implements SocialSdkContract
{

    /**
     * @var string
     */
    protected $provider = 'facebook';

    /**
     * @var array
     */
    protected $fields = [
        'id',
        'name',
        'first_name',
        'last_name',
        'picture',
        'email',
        'permissions'
    ];

    /**
     * @var array
     */
    protected $config;



    /**
     * FacebookSdk constructor.
     * @param array $config
     */
    public function __construct(array $config, $token)
    {
        parent::__construct($config);
        $this->config = $config;

        $this->setDefaultAccessToken($token);
    }


    /**
     *
     */
    public function extend_token()
    {
        $token = $this->getDefaultAccessToken();

        // Extend Token if required.
        if (! $token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauth_client = $this->getOAuth2Client();
            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (FacebookSDKException $e) {
                $token = $this->getDefaultAccessToken();
            }
        }

        $this->setDefaultAccessToken($token);
    }


    /**
     * @param $token
     * @return \Facebook\GraphNodes\GraphUser
     */
    public function get_user()
    {
        $this->extend_token();

        try {
            $response = $this->get('/me?fields=' . implode(',',$this->fields));
            return $this->map_fields($response->getGraphUser());
        } catch (FacebookSDKException $e) {
            return [
                'status'    => false,
                'error'     => [
                    'provider'  => $this->provider,
                    'message'   => $e->getMessage()
                ]
            ];
        }

    }

    /**
     * @param $user
     * @return array
     */
    public function map_fields($user)
    {
        $granted_permissions = array();
        foreach ($user->getField('permissions') as $permission){
            if($permission['status'] == 'granted') {
                $granted_permissions[] = $permission['permission'];
            }
        }

        return [
            'status'    => true,
            'provider'  => $this->provider,
            'data'      =>  [
                'profile'           =>  $this->provider,
                'id'                =>  $user['id'],
                'email'             =>  $user['email'],
                'first_name'        =>  $user['first_name'],
                'last_name'         =>  $user['last_name'],
                'display_name'      =>  $user['name'],
                'picture_url'       =>  $user['picture']['url'],
                'permissions'       =>  $granted_permissions,
                'token'             =>  $this->getDefaultAccessToken()->getValue(),
            ]
        ];
    }


    /**
     * @param $permissions
     * @return array
     */
    public function check_required_permissions($permissions)
    {
        if(count(array_intersect($this->config['permissions'], $permissions)) !=
            count($this->config['permissions']))
        {
            return [
                'status'    => false,
                'error'     => [
                    'provider'  => $this->provider,
                    'message'   => 'Token do not have required permissions.',
                    'required_permissions'  => implode(', ',$this->config['permissions'])
                ]
            ];
        }
        return [
            'status'    => true
        ];
    }


}