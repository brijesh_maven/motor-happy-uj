<?php


namespace App\Social\Interfaces;


/**
 * Interface SocialSdkContract
 * @package App\Social\Interfaces
 */
interface SocialSdkContract
{
    /**
     * @return mixed
     */
    public function get_user();

    /**
     * @return mixed
     */
    public function extend_token();

    /**
     * @param $user
     * @return mixed
     */
    public function map_fields($user);

    /**
     * @param $permissions
     * @return mixed
     */
    public function check_required_permissions($permissions);
}