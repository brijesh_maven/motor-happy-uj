<?php


namespace App\Social;
use App\Social\Interfaces\SocialSdkContract;
use App\Social\Sdks\FacebookSdk;


/**
 * Class SocialAuth
 * @package app
 */
class SocialSdk
{
    /**
     * @var SocialSdkContract
     */
    private $socialsdk;

    /**
     * @var
     */
    private $provider;

    /**
     * @var
     */
    private $token;


    /**
     * SocialSdk constructor.
     * @param $provider
     * @param $token
     */
    public function __construct($provider, $token)
    {
        $this->token = $token;
        $this->setProvider($provider);
        switch ($this->provider){
            case 'facebook':
                $socialsdk = new FacebookSdk(config('social.facebook'), $this->token);
        }
        $this->setSocialsdk($socialsdk);

    }

    /**
     * @param SocialSdkContract $socialsdk
     */
    protected function setSocialsdk(SocialSdkContract $socialsdk)
    {
        $this->socialsdk = $socialsdk;
    }

    /**
     * @param mixed $provider
     */
    protected function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return $this
     */
    public function extend_token()
    {
        $this->extend_token();
        return $this;
    }

    /**
     * @return mixed
     */
    public function get_user()
    {
        return $this->socialsdk->get_user($this->token);

    }


    /**
     * @param $permissions
     * @return mixed
     */
    public function check_permissions($permissions){
        return $this->socialsdk->check_required_permissions($permissions);
    }

}