<?php

return [

    'available_profiles' => [
        'facebook',
        'twitter',
        'instagram'
    ],

    'facebook'  => [
        'app_id' => env('FACEBOOK_APP_ID'),
        'app_secret' => env('FACEBOOK_APP_SECRET'),
        'default_graph_version' => 'v2.8',
        'permissions' => [
            'email',
            //'publish_actions',
        ],
    ]

];